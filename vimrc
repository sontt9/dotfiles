
"   set mouse=a
" end

set nocompatible

filetype plugin indent on
syntax on

scriptencoding utf-8
set hidden      " Allow buffer switching without save
set mouse=a

set ttyfast
set lazyredraw
set ttimeout
set ttimeoutlen=50

set nobackup
set noswapfile
set undofile
set undolevels=1000
set undoreload=10000
set undodir=$HOME/.vim/tmp/undo/

set incsearch
set ignorecase
set smartcase
set hlsearch

set backspace=indent,eol,start
set nojoinspaces

set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2

set autoindent
set smartindent

set foldmethod=indent
set foldlevel=5
set foldopen=block,insert,jump,mark,percent,quickfix,search,tag,undo

" Instead of reverting the cursor to the last position in the buffer, we
" set it to the first line when editing a git commit message
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

set wildmenu
set wildmode=longest:list,full
set wildignore+=node_modules/**,_build/**,deps/**,tmp/**,log/**
set wildignore+=*.so,*.swp,*.zip                 " MacOSX/Linux
set wildignore+=*.jpg,*.jpeg,*.png,*.bmp,*.gif   " Ignore binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.beam " Ignore compiled objects.
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store " Ignore version control

if has('clipboard')
  if has('unnamedplus')  " When possible use + register for copy-paste
    set clipboard=unnamed,unnamedplus
  else         " On mac and Windows, use * register for copy-paste
    set clipboard=unnamed
  endif
endif

" }}}

" PLUGINS {{{

" VimPlug {{{
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
" }}}

call plug#begin('~/.vim/bundle') " {{{

Plug 'kana/vim-textobj-user'     " Dependency
Plug 'kana/vim-textobj-entire'   " ae, ie
Plug 'kana/vim-textobj-indent'   " ai, ii
Plug 'kana/vim-textobj-fold'     " az, iz
Plug 'kana/vim-textobj-function' " af, if
Plug 'kana/vim-textobj-line'     " al, il
Plug 'glts/vim-textobj-comment'  " ac, ic

Plug 'prettier/vim-prettier', {
  \ 'do': 'npm install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown'] }

Plug 'tpope/vim-commentary' " gcc, gcap, gcip, gcii
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'tpope/vim-surround' " cs{[ , ds' ,ysiw[ , { for extra space } not
Plug 'raimondi/delimitmate'
Plug 'terryma/vim-multiple-cursors'
Plug 'rking/ag.vim'
Plug 'junegunn/vim-easy-align'
Plug 'easymotion/vim-easymotion'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'w0rp/ale'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'ludovicchabant/vim-gutentags'
Plug 'christoomey/vim-tmux-navigator'

Plug 'garbas/vim-snipmate' | Plug 'marcweber/vim-addon-mw-utils' | Plug 'tomtom/tlib_vim'

Plug 'sjl/badwolf'
Plug 'rakr/vim-one'
Plug 'NLKNguyen/papercolor-theme'
Plug 'plan9-for-vimspace/acme-colors'
Plug 'altercation/vim-colors-solarized'
Plug 'robertmeta/nofrils'

Plug 'Yggdroot/indentLine'
Plug 'itchyny/lightline.vim'
Plug 'lilydjwg/colorizer'

Plug 'leafgarland/typescript-vim'
Plug 'Quramy/vim-js-pretty-template'
Plug 'plasticboy/vim-markdown'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'evanmiller/nginx-vim-syntax'
Plug 'editorconfig/editorconfig-vim'
Plug 'aklt/plantuml-syntax'
Plug 'stephpy/vim-yaml'
Plug 'elzr/vim-json'
Plug 'toyamarinyon/vim-swift'
Plug 'moll/vim-node'
Plug 'pangloss/vim-javascript'
Plug 'elixir-lang/vim-elixir'

Plug 'vimwiki/vimwiki', { 'branch': 'dev' }

call plug#end() " }}}

let g:vimwiki_list = [{'path': '~/vimwiki/',
                       \ 'syntax': 'markdown', 'ext': '.md'}]

" }}}

" " VIEW {{{

set shortmess=aI
set number
set numberwidth=3

set splitright
set splitbelow

set scrolloff=3
set sidescrolloff=5

set showmatch
set laststatus=2
" Broken down into easily includeable segments
set statusline=%<%f\                     " Filename
set statusline+=%w%h%m%r                 " Options
"set statusline+=%{fugitive#statusline()} " Git Hotness
set statusline+=\ [%{&ff}/%Y]            " Filetype
set statusline+=\ [%{getcwd()}]          " Current dir
set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info

"set showbreak=¬     " When wrap show break marker

set list                 " Show special chars
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
" set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace

"set listchars=tab:▸\     " Tabs char
"set listchars+=trail:•   " Trailing spaces
"set listchars+=eol:¬     " End of line char
"set listchars+=precedes:«
"set listchars+=extends:» " line, long lines, non breakable
"set listchars+=nbsp:␣
"set listchars+=conceal:✘ " spaces, and conceal characters

let g:netrw_banner=0        " disable banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_liststyle=3     " tree view
let g:netrw_altv=1          " open splits to the right
let g:netrw_winsize=24


set background=dark
set t_Co=256
silent! colorscheme solarized

highlight clear SignColumn
highlight clear LineNr



" }}}

" KEY BINDING {{{

let mapleader="\<SPACE>"

inoremap jk <ESC>

" Don't use Ex Mode
nnoremap Q q
nnoremap <S-k> <nop>
nnoremap q: <silent>

nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" Fast window movement shortcuts
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

nnoremap > >>_
nnoremap < <<_
vnoremap < <gv
vnoremap > >gv

nnoremap H ^
nnoremap L g_
vnoremap H ^
vnoremap L g_

" nnoremap ; :
" nnoremap : ;

nnoremap <leader>- :exe "vertical resize -20"<cr>
nnoremap <leader>+ :exe "vertical resize +20"<cr>
nnoremap <leader>w :update<CR>
nnoremap <leader>c :bdelete<CR>

nnoremap <C-e> :e#<CR>

nnoremap <leader>/ :nohlsearch<CR>
nnoremap <leader>i mzgg=G`z
nnoremap <leader>z zMzvzz

cmap w!! w !sudo tee % > /dev/null
command! Ctags !ctags -R .

" Find merge conflict markers
nnoremap <leader>fc /\v^[<\|=>]{7}( .*\|$)<CR>

" Align GitHub-flavored Markdown tables
au FileType markdown vnoremap <leader><bslash> :EasyAlign*<Bar><Enter>

" Remove the Windows ^M - when the encodings gets messed up
nnoremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" replaces selected text with text from buffer
vnoremap p <Esc>:let current_reg = @"<CR>gvs<C-R>=current_reg<CR><Esc>

" Find and highlight all word under cursor
nnoremap <leader>fw :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

" replaces selected text with text from buffer
vnoremap p <Esc>:let current_reg = @"<CR>gvs<C-R>=current_reg<CR><Esc>

" Emacs-like bindings in Command mode
cnoremap <C-a> <Home>
cnoremap <C-e> <End>
cnoremap <C-b> <Left>
cnoremap <C-f> <Right>
cnoremap <M-b> <S-Left>
cnoremap <M-f> <S-Right>
cnoremap <C-d> <Delete>
cnoremap <C-g> <C-c>


" }}}

function! <SID>StripTrailingWhitespaces() " {{{
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  %s/\s\+$//e
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction

" PLUGINS CONFIG {{{

" Tagbar {{{

let g:tagbar_type_elixir = {
      \ 'ctagstype' : 'elixir',
      \ 'kinds' : [
      \ 'f:functions',
      \ 'functions:functions',
      \ 'c:callbacks',
      \ 'd:delegates',
      \ 'e:exceptions',
      \ 'i:implementations',
      \ 'a:macros',
      \ 'o:operators',
      \ 'm:modules',
      \ 'p:protocols',
      \ 'r:records'
      \ ]
      \ }

let g:tagbar_type_markdown = {
      \ 'ctagstype' : 'markdown',
      \ 'kinds' : [
      \ 'h:H1',
      \ 'i:H2',
      \ 'k:H3'
      \ ]
      \ }

" }}}
"
nnoremap <c-p> :FZF<cr>

if isdirectory(expand("~/.vim/bundle/vim-gutentags"))
  let g:gutentags_cache_dir = '~/.tags_cache'
endif


" Lightline
let g:lightline = {
\ 'colorscheme': 'wombat',
\ 'active': {
\   'left': [['mode', 'paste'], ['filename', 'modified']],
\   'right': [['lineinfo'], ['percent'], ['readonly', 'linter_warnings', 'linter_errors', 'linter_ok']]
\ },
\ 'component_expand': {
\   'linter_warnings': 'LightlineLinterWarnings',
\   'linter_errors': 'LightlineLinterErrors',
\   'linter_ok': 'LightlineLinterOK'
\ },
\ 'component_type': {
\   'readonly': 'error',
\   'linter_warnings': 'warning',
\   'linter_errors': 'error'
\ },
\ }
function! LightlineLinterWarnings() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%d ◆', all_non_errors)
endfunction
function! LightlineLinterErrors() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%d ✗', all_errors)
endfunction
function! LightlineLinterOK() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '✓ ' : ''
endfunction

" Update and show lightline but only if it's visible (e.g., not in Goyo)
autocmd User ALELint call s:MaybeUpdateLightline()
function! s:MaybeUpdateLightline()
  if exists('#lightline')
    call lightline#update()
  end
endfunction

if isdirectory(expand("~/.vim/bundle/vim-markdown"))
  let g:vim_markdown_conceal = 0
  let g:vim_markdown_folding_disbaled = 1
endif

if isdirectory(expand("~/.vim/bundle/vim-json"))
  let g:vim_json_syntax_conceal = 0
  nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>
endif

if isdirectory(expand("~/.vim/bundle/delimitmate"))
  let delimitMate_expand_cr = 1
endif

if isdirectory(expand("~/.vim/bundle/vim-easy-align"))
  " Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
  vmap <Enter> <Plug>(EasyAlign)
  " Start interactive EasyAlign in visual mode (e.g. vipga)
  xmap ga <Plug>(EasyAlign)
  " Start interactive EasyAlign for a motion/text object (e.g. gaip)
  nmap ga <Plug>(EasyAlign)
endif

if isdirectory(expand("~/.vim/bundle/nerdtree"))
  " map <C-E> :NERDTreeToggle<CR>
  map <leader>f :NERDTreeFind<CR>
  map <leader>e :NERDTreeToggle<CR>
  " nmap <leader>nt :NERDTreeFind<CR>

  let NERDTreeShowBookmarks=1
  let NERDTreeIgnore=['\.py[cd]$', '\~$', '\.swo$', '\.swp$', '^\.git$', '^\.hg$', '^\.svn$', '\.bzr$']
  let NERDTreeChDirMode=0
  " let NERDTreeQuitOnOpen=1
  let NERDTreeMouseMode=2
  let NERDTreeShowHidden=0
  let NERDTreeKeepTreeInNewTab=1
  let g:nerdtree_tabs_open_on_gui_startup=0
endif

if isdirectory(expand("~/.vim/bundle/ale/"))
endif

if isdirectory(expand("~/.vim/bundle/vim-easymotion/"))
  let g:EasyMotion_keys='asdfjkoweriop' " These keys are easier to type
  let g:EasyMotion_do_mapping = 0 " Disable default mappings
  let g:EasyMotion_smartcase = 1
  nmap s <Plug>(easymotion-overwin-f)
  nmap <Leader>j <Plug>(easymotion-j)
  nmap <Leader>k <Plug>(easymotion-k)
endif

if isdirectory(expand("~/.vim/bundle/ale"))
  highlight link ALEWarningSign String
  highlight link ALEErrorSign Title
  let g:ale_sign_warning = '▲'
  let g:ale_sign_error = '✗'
  let g:ale_lint_on_text_changed = 'never'
  let g:ale_lint_on_enter = 0
  let g:ale_lint_on_save = 1
  let g:ale_fixers = {
    \ 'javascript': ['eslint']
  \ }
endif


" AUTOCMD {{{

augroup reload_vimrc
  autocmd!
  autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END

augroup restore_position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$")
        \| exe "normal! g'\"" | endif
augroup END

augroup trailing_whitespace
  autocmd!
  autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()
augroup END

augroup filetype_markdown
  autocmd!
  autocmd FileType markdown setlocal nofoldenable wrap
        \ comments=b:*,b:-,b:+,n:>h
augroup END

augroup filetype_javascript
  autocmd!
  autocmd FileType javascript setlocal tabstop=2 softtabstop=2 shiftwidth=2
augroup END

" }}}

if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif



"" Reset highlight config

"let g:colors_name = "acmex"
"hi clear

"set background=light

"" Baseline

"hi Normal     ctermbg=230  ctermfg=232
"hi Folded     ctermbg=NONE ctermfg=NONE

"" *Comment  any comment
"hi Comment    ctermbg=NONE ctermfg=248

"" *Constant any constant
""    String   a string constant: "this is a string"
""    Character  a character constant: 'c', '\n'
""    Number   a number constant: 234, 0xff
""    Boolean  a boolean constant: TRUE, false
""    Float    a floating point constant: 2.3e10
"hi Constant   ctermbg=NONE ctermfg=NONE

"" *Identifier any variable name
""    Function function name (also: methods for classes)
"hi Identifier ctermbg=NONE ctermfg=NONE
"hi Function   ctermbg=NONE ctermfg=NONE cterm=bold


"" *Statement  any statement
""    Conditional  if, then, else, endif, switch, etc.
""    Repeat   for, do, while, etc.
""    Label    case, default, etc.
""    Operator "sizeof", "+", "*", etc.
""    Keyword  any other keyword
""    Exception  try, catch, throw
"hi Statement ctermbg=NONE ctermfg=NONE

"" *PreProc  generic Preprocessor
""    Include  preprocessor #include
""    Define   preprocessor #define
""    Macro    same as Define
""    PreCondit  preprocessor #if, #else, #endif, etc.

"" *Type   int, long, char, etc.
""    StorageClass static, register, volatile, etc.
""    Structure  struct, union, enum, etc.
""    Typedef  A typedef
"hi Type ctermbg=NONE ctermfg=NONE


"" *Special  any special symbol
""    SpecialChar  special character in a constant
""    Tag    you can use CTRL-] on this
""    Delimiter  character that needs attention
""    SpecialComment special things inside a comment
""    Debug    debugging statements
"hi Special ctermbg=NONE ctermfg=NONE


"" *Underlined text that stands out, HTML links

"" *Ignore   left blank, hidden  |hl-Ignore|

"" *Error    any erroneous construct
"hi Error ctermbg=NONE ctermfg=160

"" *Todo   anything that needs extra attention; mostly the
""     keywords TODO FIXME and XXX
"hi Todo ctermbg=NONE ctermfg=34

"" ColorColumn used for the columns set with 'colorcolumn'

"" Conceal   placeholder characters substituted for concealed

"" Cursor    the character under the cursor

"" CursorIM  like Cursor, but used when in IME mode |CursorIM|

"" CursorColumn  the screen column that the cursor is in when 'cursorcolumn' is set

"" CursorLine  the screen line that the cursor is in when 'cursorline' is set

"" Directory directory names (and other special names in listings)

"" DiffAdd   diff mode: Added line |diff.txt|

"" DiffChange  diff mode: Changed line |diff.txt|

"" DiffDelete  diff mode: Deleted line |diff.txt|

"" DiffText  diff mode: Changed text within a changed line |diff.txt|

"" ErrorMsg  error messages on the command line
"hi ErrorMsg ctermbg=NONE ctermfg=160

"" VertSplit the column separating vertically split windows

"" Folded    line used for closed folds

"" FoldColumn  'foldcolumn'

"" SignColumn  column where |signs| are displayed
"hi SignColumn ctermbg=NONE ctermfg=NONE

"" IncSearch 'incsearch' highlighting; also used for the text replaced with
"    ":s///c"

"" LineNr    Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
"hi LineNr     ctermbg=NONE ctermfg=137

"" MatchParen  The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|

"" ModeMsg   'showmode' message (e.g., "-- INSERT --")

"" MoreMsg   |more-prompt|

"" NonText   '~' and '@' at the end of the window, characters from 'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line).

"" Normal    normal text

"" Pmenu   Popup menu: normal item.

"" PmenuSel  Popup menu: selected item.

"" PmenuSbar Popup menu: scrollbar.

"" PmenuThumb  Popup menu: Thumb of the scrollbar.

"" Question  |hit-enter| prompt and yes/no questions

"" Search    Last search pattern highlighting (see 'hlsearch'). Also used for highlighting the current line in the quickfix window and similar items that need to stand out.

"" SpecialKey  Meta and special keys listed with ":map", also for text used to show unprintable characters in the text, 'listchars'. Generally: text that is displayed differently from what it really is.

"" SpellBad  Word that is not recognized by the spellchecker. |spell| This will be combined with the highlighting used otherwise.

"" SpellCap  Word that should start with a capital. |spell| This will be combined with the highlighting used otherwise.

"" SpellLocal  Word that is recognized by the spellchecker as one that is used in another region. |spell| This will be combined with the highlighting used otherwise.

"" SpellRare Word that is recognized by the spellchecker as one that is hardly ever used. |spell| This will be combined with the highlighting used otherwise.

"" StatusLine  status line of current window

"" StatusLineNC  status lines of not-current windows Note: if this is equal to "StatusLine" Vim will use "^^^" in the status line of the current window.

"" TabLine   tab pages line, not active tab page label

"" TabLineFill tab pages line, where there are no labels

"" TabLineSel  tab pages line, active tab page label

"" Title   titles for output from ":set all", ":autocmd" etc.

"" Visual    Visual mode selection

"" VisualNOS Visual mode selection when vim is "Not Owning the Selection". Only X11 Gui's |gui-x11| and |xterm-clipboard| supports this.

"" WarningMsg  warning messages

"" WildMenu  current match in 'wildmenu' completion
