#!/usr/bin/env bash

set -e
set -u

if [[ $(id -g) != "0" ]] ; then
  error "❯❯❯ Script must be run as ROOT."
fi

cp network-reconnect /usr/local/bin/
echo "*/5 * * * * root /usr/local/bin/network-reconnect >> /dev/null 2>&1" >> /etc/crontab

