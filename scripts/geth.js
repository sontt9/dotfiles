// https://ethereum.stackexchange.com/questions/2531/common-useful-javascript-snippets-for-geth
// https://github.com/niksmac/ethereum-scripts

function balances() {
  for (var i = 0; i < eth.accounts.length; i += 1) {
    var account = eth.accounts[i]
    var balance = web3.fromWei(eth.getBalance(account), "ether")
    console.info("eth.accounts[" + i + "]: " + account + "\tbalance: " + balance + " ETH")
  }
}

function status() {
  var lastPercentage = 0
  var lastBlocks = 0
  var timeInterval = 10000

  setInterval(function() {
    var remainingBlocks = eth.syncing.highestBlock - eth.syncing.currentBlock
    var remainingStates = eth.syncing.knownStates - eth.syncing.pulledStates
    var percentage = eth.syncing.currentBlock / eth.syncing.highestBlock * 100
    var percentagePerTime = percentage - lastPercentage
    var bps = (remainingBlocks - lastBlocks) / (timeInterval / 1000)
    var etas = 100 / percentagePerTime * (timeInterval / 1000)
    var etaM = parseInt(etas / 60, 10)
    lastPercentage = percentage
    lastBlocks = remainingBlocks

    console.info(parseInt(percentage, 10) + "% ETA: " + etaM + " munutes @ " + bps + "bps")
  }, timeInterval)
}
