# vim: fdm=marker ft=zsh:

NEWLINE='
'
PROMPT='$NEWLINE░▒▓ %n%{$fg[line]%}@%{$reset_color%}%m %~ %(?.%{$fg[lime]%}.%{$fg[red]%})❯ '

setopt NO_BEEP              # don't beep on error
setopt IGNORE_EOF           # Ignore <C-D> logout
setopt AUTO_CD           # If you type foo, and it isn't a command, and it is a directory in your cdpath, go there
setopt CDABLEVARS        # if argument to cd is the name of a parameter whose value is a valid directory, it will become the current directory
setopt PUSHD_IGNORE_DUPS # don't push multiple copies of the same directory onto the directory stack

# ===== Expansion and Globbing
setopt EXTENDED_GLOB # treat #, ~, and ^ as part of patterns for filename generation

# ===== History
setopt EXTENDED_HISTORY       # Write the history file in the ":start:elapsed;command" format.
setopt APPEND_HISTORY         # Allow multiple terminal sessions to all append to one zsh command history
setopt INC_APPEND_HISTORY     # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY          # Share history between all sessions.
setopt HIST_IGNORE_DUPS       # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS   # Delete old recorded entry if new entry is a duplicate.
setopt HIST_IGNORE_SPACE      # Don't record an entry starting with a space.
setopt HIST_EXPIRE_DUPS_FIRST # Expire duplicate entries first when trimming history.
setopt HIST_FIND_NO_DUPS      # Do not display a line previously found.
setopt HIST_SAVE_NO_DUPS      # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS     # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY            # Don't execute immediately upon history expansion.

# ===== Completion
setopt ALWAYS_TO_END    # When completing from the middle of a word, move the cursor to the end of the word
setopt AUTO_MENU        # show completion menu on successive tab press. needs unsetop menu_complete to work
setopt AUTO_NAME_DIRS   # any parameter that is set to the absolute name of a directory immediately becomes a name for that directory
setopt COMPLETE_IN_WORD # Allow completion from within a word/phrase

unsetopt MENU_COMPLETE # do not autoselect the first completion entry

# ===== Correction
unsetopt CORRECT_ALL # spelling correction for arguments
unsetopt CORRECT     # spelling correction for commands

# ===== Prompt
setopt PROMPT_SUBST # Enable parameter expansion, command substitution, and arithmetic expansion in the prompt
# setopt transient_rprompt # only show the rprompt on the current prompt

# ===== Scripts and Functions
setopt MULTIOS # perform implicit tees or cats when multiple redirections are attempted

HISTFILE="$HOME/.zsh-history"
HISTSIZE=10000000
SAVEHIST=10000000
HISTORY_IGNORE="(la*|ll*|ls*|cd*|fg|bg|exit\
  |startx\
  |zplug*\
  |rand*\
  |npm install*\
  |pass*\
  |touch*\
  |mkdir*\
  |rm*\
  |less*\
  |vim*\
  |git*|gci*|gca*|gaa|gph|gpl|gst|grh|glog\
  |htop|top|ranger|nvm|node|iex\
  )"

export TERM=rxvt-256color
export CLICOLOR=1
export LSCOLORS=Gxfxcxdxbxegedabagacad
export GREP_COLOR='3;33' # Enable color in grep

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

export PATH=$PATH:$HOME/.dotfiles/bin

## ZPLUG {{{

export ZPLUG_HOME=$HOME/.zplug

if [[ ! -d $ZPLUG_HOME ]]; then
  git clone https://github.com/zplug/zplug $ZPLUG_HOME
  source $ZPLUG_HOME/init.zsh && zplug update --self
fi

source $ZPLUG_HOME/init.zsh

zplug "psprint/history-search-multi-word"
zplug "zsh-users/zsh-syntax-highlighting"
zplug "hlissner/zsh-autopair"

if ! zplug check --verbose; then
  zplug install
fi

zplug load

# }}}

## BINDKEYS {{{

stty -ixon # Disable Ctrl+S, Ctrl+Q

bindkey -e # Emacs Style
bindkey '^R' history-search-multi-word

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

bindkey '^P' up-line-or-beginning-search
bindkey '^N' down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

autoload -U edit-command-line # Enable CTRL-X-E to edit command line
zle -N edit-command-line      # Allow CTRL+XE | CTRL+X CTRL+E

bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line

function fancy-ctrl-z () {
  if [[ $#BUFFER -eq 0 ]]; then
    BUFFER="fg"
    zle accept-line
  else
    zle push-input
    zle clear-screen
  fi
}

zle -N fancy-ctrl-z
bindkey '^Z' fancy-ctrl-z

# }}}

## ALIASES {{{

# GNU and BSD (MacOS) ls flags compatible
ls --version &> /dev/null
if [ $? -eq 0 ]; then
  lsflags="--color --group-directories-first -F"
else
  lsflags="-GF"
  export CLICOLOR=1
fi

alias ls="ls ${lsflags}"
alias ll="ls ${lsflags} -lh"
alias la="ls ${lsflags} -lha"
alias l.="ls ${lsflags} -lhd .??*"

alias ..='cd ..'
alias ...='cd ../..'
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd-='cd -'

alias g='grep'
alias j='jobs'
alias h='head'
alias t='tail -f'

alias v='vim'
alias vi='vim'
alias mv='mv -v'
alias rm='rm -vr'
alias cp='cp -vr'
alias mkdir='mkdir -vp'
alias ping='ping -c 5'
alias tail='tail -f'
alias less='less -FN'

alias get='http -v get'
alias post='http -v post'
alias put='http -v put'
alias delete='http -v delete'

alias dug='du -h | grep "^[0-9.]*G" | sort -rn'
alias duc='du -sh * | sort -rh'

alias f='find . -iname'
alias ff='find . -type f -iname'
alias fd='find . -type d -iname'

if command -v xclip >/dev/null; then
  alias xcopy='xclip -selection clipboard -o'
  alias xpaste='xclip -selection clipboard -i'
fi

alias gst='git status --short --untracked-files'
alias ga='git add'
alias gaa='git add --all'
alias gci='git commit --verbose'
alias gca='git commit --verbose --amend'
alias gco='git checkout'
alias gcb='git checkout -b'
alias gb='git branch'
alias gba='git branch --all'
alias gf='git fetch'
alias gfa='git fetch --all --prune'
alias grs='git reset --soft'
alias grh='git reset --hard'
alias gcl='git clean -fd'
alias gpl='git pull'
alias gph='git push'
alias gsa='git stash save --include-untracked'
alias gundo='git reset --soft HEAD~1'
alias gundopush='git push -f origin HEAD^:master'
alias gdf='git difftool'
alias glog="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gloga="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --all"

## }}}

# Func {{{

function rand(){
  < /dev/urandom tr -dc '?<>|#%^&*()-=_+,./;:~qwertyupasdfghkzxcvbnmQWERTYUPASDFGHKZXCVBNM23456789' | head -c${1:-$1};echo;
}

function json {
  # Syntax-highlight JSON strings or files
  # Usage: `json '{"foo":42}'` or `echo '{"foo":42}' | json`
  if [ -t 0 ]; then # argument
    python -mjson.tool <<< "$*" | pygmentize -l javascript
  else # pipe
    python -mjson.tool | pygmentize -l javascript
  fi
}

function csv2json {
  python -c "import csv,json;print json.dumps(list(csv.reader(open('$1'))))"
}

function note() {
  vim ~/vimwiki/note-$(date '+%Y-%m-%d').md
}

function transfer() {
  if [ $# -eq 0 ]; then
    echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1;
  fi

  tmpfile=$( mktemp -t transferXXX );

  if tty -s; then
    basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g');
    curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile;
  else
    curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ;
  fi;

  cat $tmpfile
  cat $tmpfile | xclip -selection clipboard -i
  rm -f $tmpfile;
}

function any() {
  # any function from http://onethingwell.org/post/14669173541/any
  # search for running processes
  emulate -L zsh
  unsetopt KSH_ARRAYS
  if [[ -z "$1" ]] ; then
    echo "any - grep for process(es) by keyword" >&2
    echo "Usage: any " >&2 ; return 1
  else
    ps xauwww | grep -i --color=auto "[${1[1]}]${1[2,-1]}"
  fi
}

# }}}

# (s)ave or (i)nsert a directory.
save() { pwd > ~/.save_dir ; }
insert() { cd "$(cat ~/.save_dir)" ; }

if [[ `uname` == 'Linux' ]]; then
  export OS=linux
  function nvm() {
     export NVM_DIR="$HOME/.nvm"
     [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
     [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
  }
elif [[ `uname` == 'Darwin' ]]; then
  export OS=osx
  export NVM_DIR=~/.nvm
  source $(brew --prefix nvm)/nvm.sh
fi



export FZF_DEFAULT_COMMAND='ag -g ""'
export FZF_DEFAULT_OPTS='--height 40% --reverse'
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


