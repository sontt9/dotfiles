#!/usr/bin/env bash

git clone https://gitlab.com/sontt9/dotfiles $HOME/.dotfiles

ln -sf $HOME/.dotfiles/Xresources $HOME/.Xresources
ln -sf $HOME/.dotfiles/urxvt $HOME/.urxvt
ln -sf $HOME/.dotfiles/zshrc $HOME/.zshrc
ln -sf $HOME/.dotfiles/gitconfig $HOME/.gitconfig
ln -sf $HOME/.dotfiles/editorconfig $HOME/.editorconfig
mkdir $HOME/.vim/colors
ln -sf $HOME/.dotfiles/vim/colors/minimal.vim $HOME/.vim/colors/minimal.vim
ln -sf $HOME/.dotfiles/vimrc $HOME/.vimrc
ln -sf $HOME/.dotfiles/vim/snippets $HOME/.vim/
